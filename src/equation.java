
public class equation {

	public static void main(String[] args) {

		double a, b, c, delta, x1, x2, x3;
		boolean condition;
// delta = 0	
		
		a = 20;
		b = 80;
		c = 80;
		
// delta > 0
		// a = 5 
		// b = 8
		// c = 3
		
// delta < 0
		// a = 40
		// b = 80
		// c = 80

		delta = (b * b) - (4 * a * c);
		condition = (a != 0);

		if (condition) {

			if (delta > 0) {

				x1 = (-b - Math.sqrt(delta)) / (2 * a);
				x2 = (-b + Math.sqrt(delta)) / (2 * a);
				System.out.print("Poss�de une ou deux solutions r�els x1 = " + x1 + " x2 = " + x2);
			}

			else if (delta == 0) {

				x3 = ((-b) / (2 * a));
				System.out.print("Poss�de une solution r�el x3 = " + x3);
			}

			else {

				System.out.print("Aucune solution r�el");
			}
		} else {
			System.out.print("A diff�rent de 0");
		}
	}

}
